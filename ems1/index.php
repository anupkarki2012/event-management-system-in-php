<?php

$con = mysqli_connect("localhost","root","","ems");
                    //servername,username,password,databasename//
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eventbooking</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

 <!--==========================
  HEADER SECTION
  ============================-->
<div class="header">
  <div class="container">
   <nav class="navbar navbar-default navbar-fixed-top">
     <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand"href="index.php"><img src="img/logo.png" alt="" title="" />Event<br>booking</a>
   </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
         <li class=""><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="event.php">View Events</a></li>
        <li><a href="about.php">About us</a></li>
        <li><a href="contactform.php">contact us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="signup.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Signup</a></li>
        <li><a href="login.php"><span class="glyphicon  glyphicon-log-in" aria-hidden="true"></span>Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
  </nav>
 </div>
</div>

<!--==========================
  INTRO  SECTION
  ============================-->
<div class="jumbotron">
   <div class="container">
  <h1 >Event Booking System</h1>
  <p>We are ready to Sell your event in Large Mass in a affordable price!!!</p>
  <center><p><a class="btn btn-primary btn-lg" href="about.php" role="button">Learn more</a></p></center>
 </div>
</div>

<!--==========================
  Event Post SECTION
============================-->
<div class="event-post">
  <div class="container">
    <h2>Our Latest Events</h2>
    <div class="row">
   <?php 
    $query = "select * from eventpost order by rand() LIMIT 0,4";
    $run = mysqli_query($con,$query);
      while ($row=mysqli_fetch_array($run)){
        $post_id= $row['id'];
        $title = $row['title'];
        $image_name =$row['image'];
        $location = $row['location'];
        $date = $row["date"];
        $descp = substr($row['description'],0,120);
        $price = $row['price'];
       ?>
           <div class="col-md-3">
           <h3 class="event-title"><?php echo $title ?></h3>
         <img src="img/<?php echo $image_name;?>" class="img-responsive" alt="Responsive image">
        <h4 class="location">Location:<?php echo $location ?></h4>
      <p class="date">Event Date:<?php echo $date ?></p>
      <p class="price">Rs.<?php echo $price ?></p>
      <p class="description"><?php echo $descp ?></p>
     <p><a class="book-event" href="booking.php?id=<?php echo($post_id);?>"role="button">Book Now</a></p>
  </div>
  <?php   } ?>
    </div>
  </div>
</div>

<!--==========================
      Call To Action Section
============================-->
    <div class="call-to-action">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Contact us for you any Events related Post !!! </h3>
            <p class="cta-text"> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="contactform.php">Get Start Now</a>
          </div>
        </div>
        </div>
      </div>
    </div>

<!--==========================
      Footer Section
============================-->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h3>About us</h3>
         <p class="fotter-text">We are the student of Aryan School of Enineering and Management who developed this website Event Management System.So, this website help you to manage various types of events in near future. </p>
      </div>
      <div class="col-md-3">
        <h3>UseFul links</h3>
        <li><a href="#">Term and Condition</a></li>
        <li><a href="#">Pivacy & Policies</a></li>
        <li><a href="#">About Company</a></li>
        <li><a href="#">Event Management</a></li>
        <li><a href="#">Call To support</a></li>
      </div>
      <div class="col-md-3">
         <h3>Support</h3>
         <h5>Address:Pepsicola-35</h5>
         <h5>Location:Kathmandu,Nepal</h5>
         <h5>Email:support@eventbooking.com</h5>
         <h5>Phone No:9804714399/01-5541234</h5>
      </div>
      <div class="col-md-3">
       <h3>Our Newsletters</h3>
        <p class="fotter-text">Hello, Everyone this is our beta version of website for managing various types events that going to held in all over the world.We usually upload event related post on this website.</p>
       </ul>
      </div>
    </div>
  </div>
</div>

<!--==========================
  CopyRight Footer Section
============================-->
<div class="c-footer">
  <div class="container">
    <center>&copy; Copyright <strong>eventbooking.com</strong>. All Rights Reserved - 2019</center> 
      </div>
     <center>Developed by <a class="f-text" href="http://www.anupkarki.com.np/?i=2">Aryan School Project Group A</a></center>
  </div>
</div>


<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});
    </script>
  </body>
</html>