<?php
error_reporting(0);
if(isset($_POST['submit']))
{
  $name=$_POST['name'];
  $email=$_POST['email'];
  $subject=$_POST['subject'];
  $message=$_POST['message'];
  $to='anupkarki2012@gmail.com';
  $sub="Form Submission".$subject;
  $sub2="Copy of Form Submission:".$subject;
  $msg="Name:".$name."\n\n"."email:".$subject."\n\n". "message:\n\n".$message;
  $msg2="Here is the copy of message:\n\n"."Name:".$name."\n\n"."email:".$subject."\n\n". "message:\n\n".$message;
  $headers="From:".$email;
  $headers2="From".$to;
    mail($to, $subject ,$msg, $headers);
    mail($email, $subject2, $msg2, $headers2);
    echo "<h2>Mail Sent Thank You!".$name." We will Contact You soon!!.</h2>";
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eventbooking</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--==========================
  HEADER SECTION
  ============================-->
<div class="header">
  <div class="container">
   <nav class="navbar navbar-default navbar-fixed-top">
     <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand"href="index.php"><img src="img/logo.png" alt="" title="" />Event<br>booking</a>
   </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="event.php">View Events</a></li>
        <li><a href="about.php">About us</a></li>
        <li><a href="contactform.php">contact us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="signup.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Signup</a></li>
        <li><a href="login.php"><span class="glyphicon  glyphicon-log-in" aria-hidden="true"></span>Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
  </nav>
 </div>
</div>
<!--==========================
  Contact us Section
============================-->
<div class="contact-us">
  <div class="container">
    <h2>Contact Us</h2>
    <form action="" method="POST">
    <center><div class="form-area">
       <input name="name" type="text" placeholder="Your name" class="form-control" required>
       <input name="email" type="text" placeholder="Your email" class="form-control" required>
       <input name="subject" type="text" placeholder=" Your Subject" class="form-control"required>
       <textarea name="message" placeholder="Please enter your message here..." rows="5"class="form-control" required></textarea>
       <input type="submit" class="button" value="submit" name="submit">
     </div></center>
  </div>
 </form>
</div>

<!--==========================
  CopyRight Footer Section
============================-->
<div class="c-footer">
  <div class="container">
    <center>&copy; Copyright <strong>eventbooking.com</strong>. All Rights Reserved - 2019</center> 
      </div>
     <center>Developed by <a class="f-text" href="http://www.anupkarki.com.np/?i=2">Aryan School Project Group A</a></center>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>