
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eventbooking</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

   <body>
  <!--==========================
  HEADER SECTION
  ============================-->
<div class="header">
  <div class="container">
   <nav class="navbar navbar-default navbar-fixed-top">
     <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand"href="index.php"><img src="img/logo.png" alt="" title="" />Event<br>booking</a>
   </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="event.php">View Events</a></li>
        <li><a href="about.php">About us</a></li>
        <li><a href="contactform.php">contact us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="signup.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Signup</a></li>
        <li><a href="login.php"><span class="glyphicon  glyphicon-log-in" aria-hidden="true"></span>Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
  </nav>
 </div>
</div>

<!--==========================
  About Section
============================-->
<div class="about">
  <div class="container">
    <h1>About Us</h1>
    <p> We are the student of Aryan School of Enineering and Management who developed this website Event Management System.So, this website help various client's and user to manage  and book various types of events in near future from any place of the world.</p>
  </div>
</div>
<div class="team">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
       <center><img src="img/team.png" class="img-responsive" alt="Responsive image"></center>
        <h2>Anup Karki</h2>
        <p>#Position</p>
         </div>
            <div class="col-md-4">
              <center><img src="img/team.png" class="img-responsive" alt="Responsive image"></center>
              <h2>Krishna Maharjan</h2>
              <p>#Position</p>
         </div>
            <div class="col-md-4">
              <center><img src="img/team.png" class="img-responsive" alt="Responsive image"></center>
              <h2>Abishek Gautam</h2>
              <p>#Position</p>
         </div>
    </div>
  </div>
</div>

<!--==========================
  CopyRight Footer Section
============================-->
<div class="c-footer">
  <div class="container">
    <center>&copy; Copyright <strong>eventbooking.com</strong>. All Rights Reserved - 2019</center> 
      </div>
     <center>Developed by <a class="f-text" href="http://www.anupkarki.com.np/?i=2">Aryan School Project Group A</a></center>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>