
<?php 
      session_start();
      $name=$_SESSION['email'];
       if(strlen($name)==0)
       {
            header("location:index.php");
       }
$con=mysqli_connect("localhost","root","","ems");
                    //servername,username,password,databasename//
   @$status = $_SESSION['status'];
   $smsg = "Pending".$status;
   $postby=$_SESSION['email'];
   $userd="User: ".$postby;

if(isset($_POST['submit']))
{
      $title = $_POST['title'];
      $image_name = $_FILES['image']['name'];
      $image_type = $_FILES['image']['type'];
      $image_size = $_FILES['image']['size'];
      $image_temp = $_FILES['image']['tmp_name'];
      $location= $_POST['location'];
      $date = date('y-m-d-');
      $descp = $_POST['description'];
      $price = $_POST['price'];

   if($title=='' or $location=='' or $date=='' or $descp=='' or $price=='')
   {
     echo "<script>alert('Any Field is Empty')</script>";
    exit();
   }
   if($image_type=="img/jpeg" or $image_type=="img/jpg" or $image_type=="img/png" or $image_type=="img/gif")
   {
      if($image_size<=50000)
    {
       move_uploaded_file($image_temp, "img/$image_name");
     }
    else{
     echo"<script>alert('Image is larger,Only 50kb Size is allowed')</script>";
    }
   }
   // else{
   //   echo"<script>alert('image type is invalid')</script>";
   // }
   
  $query = "insert into userpost(title,image,location,date,description,price,postby,status)VALUES('$title','$image_name','$location','$date','$descp','$price','$userd','$smsg')";
  if(mysqli_query($con,$query))
  {
    echo"<script>alert('Event Has Been Sucessfully Upload!!!')</script>";
  }
}
 
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EventManagement</title>

    <!-- Bootstrap -->
     <link rel="stylesheet" type="text/css" href="css/style.css">
     <link href="css/bootstrap.min.css" rel="stylesheet">
   

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <!==========================
        Admin SECTION
  ============================-->
<div class="header-u-p">
      <p style="color: #fff;text-align: center;">User Panel | Event Management System</p>
    </div>
<div class="container-fluid">
  <div class="row">
    <!--side-menus-->
      <div class="side-menus col-md-2">
      <div class="title">
      <a href="dashboard.php"><h1>Dashboard</h1></a>
    </div>
       <div class="menu-bar">
              <ul>
                <li><a href="createevent.php">Add event</a></li>
                <li><a href="logout.php">Log out</a></li>
              </ul>
             </div>
        </div>
    <!--main-content-section-->
    <div class="box-content col-md-10" id="visit">
     <div class="event-post" style="text-align: center;">
        <h1 class="add-event">Add New Event</h1>
      <form action="" method="POST" enctype="multipart/form-data">
      <input class="form-control" type="text" name="title" placeholder="Enter The Event title" autofocus required>
      <input class="picture"type="file" name="image">
      <input class="form-control" type="text" name="location" placeholder="Enter The Location Name"autofocus required>
      <input type="date" name="date" class="form-control">
      <textarea name="description" placeholder="Please enter your event description here..." rows="5"class="form-control" autofocus required></textarea>
      <input class="form-control" type="number" name="price" placeholder="Enter The Price"autofocus required>
      <input type="submit" class="upload" value="Upload Event" name="submit">
    </form>
    </div>
  </div>
</div>

  </head>
    <body>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>