<?php
session_start();
$con = mysqli_connect("localhost","root","","ems");
                    //servername,username,password,databasename//
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eventmanagement</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <!--==========================
  HEADER SECTION
  ============================-->
<div class="header">
  <div class="container">
   <nav class="navbar navbar-default navbar-fixed-top">
     <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand"href="index.php"><img src="img/logo.png" alt="" title="" />Event<br>booking</a>
   </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
        <li><a href="event.php">View Events</a></li>
        <li><a href="about.php">About us</a></li>
        <li><a href="contactform.php">contact us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="signup.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Signup</a></li>
        <li><a href="login.php"><span class="glyphicon  glyphicon-log-in" aria-hidden="true"></span>Login</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
  </nav>
 </div>
</div>

<!--==========================
  Event Post SECTION
============================-->
<div class="info">
  <div class="container">
  <h5 class="cs-info">Our Events details are given below section  all the event that will organized soon.We are accepting all kind of events that are held within the country as well as outside the country too.So,Please Pick out the event's that are held within your area and reserve the ticket soon!!!!</h5>
  </div>
</div>
<div class="event-post">
  <div class="container">
    <h2>Our Events</h2>
    <div class="row">
    <?php 
    $query = "select * from eventpost order by rand() LIMIT 0,6";
    $run = mysqli_query($con,$query);
      while ($row=mysqli_fetch_array($run)){
        $post_id= $row['id'];
        $title = $row['title'];
        $image_name =$row['image'];
        $location = $row['location'];
        $date = $row["date"];
        $descp = substr($row['description'],0,150);
        $price = $row['price'];
       ?>
           <div class="col-md-3">
           <h3 class="event-title"><?php echo $title ?></h3>
         <img src="img/<?php echo $image_name;?>" class="img-responsive" alt="Responsive image">
        <h4 class="location"> Location:<?php echo $location ?></h4>
      <p class="date">Event Date:<?php echo $date ?></p>
      <p class="price">Rs.<?php echo $price ?></p>
      <p class="description"><?php echo $descp ?></p>
     <p><a class="book-event" href="booking.php?id=<?php echo($post_id);?>"role="button">Book Now</a></p>
  </div>
  <?php   } ?>
           
   </div>
  </div>
</div>

<!--==========================
  CopyRight Footer Section
============================-->
<div class="c-footer">
  <div class="container">
    <center>&copy; Copyright <strong>eventbooking.com</strong>. All Rights Reserved - 2019</center> 
      </div>
     <center>Developed by <a class="f-text" href="http://www.anupkarki.com.np/?i=2">Aryan School Project Group A</a></center>
  </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>