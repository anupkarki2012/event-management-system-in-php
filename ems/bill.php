
<?php

$con = mysqli_connect("localhost","root","","ems");
                    //servername,username,password,databasename//
$sql="select * from eventpost join bookingorder";
$res=mysqli_query($con,$sql);
if ($res) {
  # code...
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eventbooking</title>
    <style type="text/css">
      .invoice h1{
        text-align: center;
        font-size: 26px;
        font-weight: bold;
        color: #31708f;
      }
      .invoice p{
        text-align: center;
        font-weight: bold;
      }
      .bill-info p
      {
        text-align: left;
        font-weight: bolder;
        margin-left: 30%;
      }
       .bill-note p
      {
        text-align: left;
        font-weight: bolder;
        margin-left: 32%;
      }
      .bills{
        background: pink;
      }
    </style>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

 <!--==========================
  BILL SECTION
  ============================-->
<div class="container">
<div class="bills">
<div class="invoice">
  <div class="container">
    <h1>Event management System</h1>
    <p>Contact:info@eventmanagement.com  <span>/  Phone:+977-9804714399</span></p>
    <p>Location:Pepsicola , Kathmandu Nepal</p>
  </div>
</div>
  <?php
  if($row=mysqli_fetch_array($res)) {
    # print_r($row)...
    ?>
<div class="bill-info">
  <div class="container">
  <p>Name:<?php echo $row["name"] ?></p>
  <p>Email:<?php echo $row["email"] ?></p>
  <p>Address:<?php echo $row["address"] ?></p>
  <p>Event Title:<?php echo $row["title"] ?></p>
  <p>Date:<?php echo $row["date"] ?></p>
  <p>Location:<?php echo $row["location"] ?></p>
  <p>Paid Amount:Rs.<?php echo $row["price"] ?></p>
  <center><a style="color: green;font-weight: bold;font-size: 18px;" href='completed.php?id=<?php echo $row['id'];?>'>COMPLETED</a></center>
  </div>
</div>
<?php
  }
    //closing of if//
    ?>
  <?php}?>
<div class="bill-note">
  <div class="container">
  <p>Thank You for connectinng with Event Management System!!</p>
  </div>
</div>
</div>
</div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>