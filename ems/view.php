<?php
      session_start();
      $name=$_SESSION['username'];
       if(strlen($name)==0){
            header("location:index.php");
       }
?>
 <?php 
$con=mysqli_connect("localhost","root","","ems");
                   //servername,username,password,databasename//
$sql="select * from eventpost";
$res=mysqli_query($con,$sql);
if ($res) {
  # code...
}
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EventManagement</title>
    <style type="text/css">
      .content-table{
        boarder-collaps: collapse;
        margin:25px 0;
        font-size: 15px;
        min-width:400px;
      }
      .content-table th{
        background-color: #009879;
        color: #fff;
        text-align: left;
        padding-top: 5px;
        padding-bottom: 5px;
        font-weight: bold;
      }
      .content-table tr{
        padding-left:10px;
        text-align: left;
      }
      table{
        border-bottom: 1px solid #dddddd;
      }
      .content-table tr:nth-of-type(even)
      {
      background:#f3f3f3; 
      }
      .content-table tr:last-of-type
      {
        border-bottom: 2px solid #009879;
      }
      .content-table td{
        padding-top: 10px;
        padding-bottom: 10px;
        font-weight: bold;
      }
    </style>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->

  </head>
    <body>
  <!==========================
        Admin SECTION
  ============================-->
<div class="header">
   <div class="tt">
      <p style="color: #fff;text-align: center;">Admin Panel | Event Management System</p>
    </div>
  </div>
<div class="container-fluid">
  <div class="row">
    <!--side-menus-->
      <div class="side-menus col-md-2">
      <div class="title">
      <a href="dashboard.php"><h1>Dashboard</h1></a>
    </div>
       <div class="menu-bar">
              <ul>
                <li><a href="addevent.php">Add event</a></li>
                <li><a href="view.php">Event List</a></li>
                <li><a href="vieworder.php">Order List</a></li>
                <li><a href="userpost.php">User Post</a></li>
                <li><a href="logout.php">Log out</a></li>
              </ul>
             </div>
    </div>
    <!--main-content-section-->
    <div class="box-content col-md-10" id="visit">
      <table class="content-table" style="border-collapse: collapse;width: 100%;">
  <tr>
    <th>ID</th>
    <th>TITLE</th>
    <th>IMAGE</th>
    <th>LOCATION</th>
    <th>DATE</th>
    <th>PRICE</th>
    <th>POSTBY</th>
    <th style="text-align: center;" colspan="2">ACTION's</th>
  </tr>
  <?php
  while($row=mysqli_fetch_array($res)) {
    # print_r($row)...
    ?>
    <tr>
      <td height="15px"><?php echo $row["id"] ?></td>
      <td><?php echo $row["title"] ?></td>
      <td><?php echo $row["image"] ?></td>
      <td><?php echo $row["location"] ?></td>
      <td><?php echo $row["date"] ?></td>
      <td><?php echo $row["price"] ?></td>
      <td><?php echo $row["postby"] ?></td>
      <td><a href='update-adminpost.php?id=<?php echo $row['id'];?>'>Update</a></td>
      <td><a href='delete.php?id=<?php echo $row['id'];?>'>Delete</a></td>

    </tr>
    <?php
  }
    //closing of while loop//
    ?>
  </table>
  <?php}?>
    </div>
  </div>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>