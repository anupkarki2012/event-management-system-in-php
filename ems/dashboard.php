
<?php
      session_start();
      $name=$_SESSION['username'];
       if(strlen($name)==0){
            header("location:index.php");
       }
?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EventManagement</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <!==========================
        Admin SECTION
  ============================-->
<div class="header">
   <div class="tt">
      <p style="color: #fff;text-align: center;">Admin Panel | Event Management System</p>
    </div>
  </div>
<div class="container-fluid">
  <div class="row">
    <!--side-menus-->
      <div class="side-menus col-md-2">
      <div class="title">
      <a href="dashboard.php"><h1>Dashboard</h1></a>
    </div>
       <div class="menu-bar">
              <ul>
                <li><a href="addevent.php">Add event</a></li>
                <li><a href="view.php">Event List</a></li>
                <li><a href="vieworder.php">Order List</a></li>
                <li><a href="userpost.php">User Post</a></li>
                <li><a href="logout.php">Log out</a></li>
              </ul>
             </div>
    </div>
    <!--main-content-section-->
    <div class="box-content col-md-10" id="visit">
      <p style="color: #000;text-align: center; font-size: 30px; margin-top: 240px;">Welcome To Admin Dashboard</p>
    </div>
  </div>
</div>

  </head>
    <body>
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>