-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2019 at 05:26 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `id` int(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminlogin`
--

INSERT INTO `adminlogin` (`id`, `username`, `password`) VALUES
(1, 'emsadmin', 'emspass');

-- --------------------------------------------------------

--
-- Table structure for table `bookingorder`
--

CREATE TABLE `bookingorder` (
  `id` int(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `price` varchar(200) NOT NULL,
  `payoption` varchar(200) NOT NULL,
  `payid` varchar(200) NOT NULL,
  `b_status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookingorder`
--

INSERT INTO `bookingorder` (`id`, `name`, `email`, `address`, `phone`, `price`, `payoption`, `payid`, `b_status`) VALUES
(1, 'Anup Jung Karki', 'anupkarki2012@gmail.com', 'Pepsicola,Kathmandu', '9804714399', '1000', 'Esewa', '9804714399', '');

-- --------------------------------------------------------

--
-- Table structure for table `eventpost`
--

CREATE TABLE `eventpost` (
  `id` int(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(1000) NOT NULL,
  `price` varchar(200) NOT NULL,
  `postby` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventpost`
--

INSERT INTO `eventpost` (`id`, `title`, `image`, `location`, `date`, `description`, `price`, `postby`) VALUES
(1, 'Rock For Nepal', 'albatross.jpg', 'Kathmandu, Nepal', '2019-11-20', 'Hello, Everyone, one of the big events is going to held on 11/22/2019 on Sunday at Peoples Plaza Lajimpat, Kathmandu. One of the most favorite bands of Nepal Albatross is going to perform on the stage sharply at 10 A.M onward So, if you like music event the Book the ticket from our website and reserved the seat soon and get a chance to watch a live concert. If any inquiry the contact our team. We are ready to reply to your question at any time as soon as possible.', '1000', 'User: emsadmin'),
(2, 'Mantra Band Live', 'mantra.jpg', 'Pokhara Bazzar', '2019-11-20', 'Hello, Everyone, one of the big events is going to held on 11/22/2019 on Sunday at Pokhara Bazzar, Pokhara. One of the most favorite bands of Nepal Mantra Band is going to perform on the stage sharply at 11 A.M onward So, if you like music event the Book the ticket from our website and reserved the seat soon and get a chance to watch a live concert. If any inquiry the contact our team. We are ready to reply to your question at any time as soon as possible.', '3000', 'User: emsadmin'),
(3, 'Star Eve Concert', 'nabin k bhattrai.jpg', 'Kamalpokhari, Kathmandu', '2019-11-20', 'Hello, Everyone, one of the big events is going to held on 11/22/2019 on Sunday at Kamalpokhari, Kathmandu. One of the most favorite Singer of Nepal Nabin k. Bhattrai is going to perform on the stage sharply at 10 A.M onward So, if you like music event the Book the ticket from our website and reserved the seat soon and get a chance to watch a live concert. If any inquiry the contact our team. We are ready to reply to your question at any time as soon as possible.', '5000', 'User: emsadmin'),
(4, 'Basant Utsav', 'Basant_Utsav.jpg', 'Skkim , India', '2019-11-20', 'Hello, Everyone, one of the big events is going to held on 11/22/2019 on Sunday at Sikkim,India. One of the most favorite Singer of Nepal /India Bipul chhetri is going to perform on the stage sharply at 10 A.M onward So, if you like music event the Book the ticket from our website and reserved the seat soon and get a chance to watch a live concert. If any inquiry the contact our team. We are ready to reply to your question at any time as soon as possible.', '500', 'User: emsadmin'),
(5, 'Voice of Nepal', 'the-voice-of-nepal-uk.jpg', 'Tinkune, Kathmandu', '2019-11-20', 'Hello, Everyone, one of the big events is going to start on 11/22/2019 on Sunday at Tinkune, Kathmandu. One of the most favorite show of Nepali Voice of nepal going to perform on the stage sharply at 10 A.M onward So, if you like music event the Book the ticket from our website and reserved the seat soon and get a chance to watch a live concert. If any inquiry the contact our team. We are ready to reply to your question at any time as soon as possible.', '4000', 'Admin: emsadmin');

-- --------------------------------------------------------

--
-- Table structure for table `uregister`
--

CREATE TABLE `uregister` (
  `id` int(100) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(123) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `uregister`
--

INSERT INTO `uregister` (`id`, `email`, `password`) VALUES
(1, 'anupkarki2012@gmail.com', 'Simkharka5'),
(2, 'karkivai@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `userpost`
--

CREATE TABLE `userpost` (
  `id` int(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(1000) NOT NULL,
  `price` varchar(200) NOT NULL,
  `postby` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userpost`
--

INSERT INTO `userpost` (`id`, `title`, `image`, `location`, `date`, `description`, `price`, `postby`, `status`) VALUES
(2, 'Nepal ,Idol', 'nepalidol.jpg', 'Baneshwor, Kathmandu', '2019-11-20', 'Hello, Everyone, one of the big events is going to held on 11/22/2019 on Sunday at Peoples Baneshwor, Kathmandu. One of the most favorite reality show is  going to start on the stage sharply at 10 A.M onward So, if you like music event the Book the ticket from our website and reserved the seat soon and get a chance to watch a live concert. If any inquiry the contact our team. We are ready to reply to your question at any time as soon as possible.', '2000', 'Admin: karkivai@gmail.com', 'Pending');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookingorder`
--
ALTER TABLE `bookingorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventpost`
--
ALTER TABLE `eventpost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uregister`
--
ALTER TABLE `uregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userpost`
--
ALTER TABLE `userpost`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bookingorder`
--
ALTER TABLE `bookingorder`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `eventpost`
--
ALTER TABLE `eventpost`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `uregister`
--
ALTER TABLE `uregister`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `userpost`
--
ALTER TABLE `userpost`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
